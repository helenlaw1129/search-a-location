# Search A Location


## Getting Started


Then follow these steps to install and run the application:

1. Clone this repo to your local machine.
2. Start the PostgreSQL server and import `/backend/dump-search-a-location-202204170141.sql` to a local database
3. Add a `.env` file with database connect information to `/backend` (can copy the `/backend/.env.sample` template)
4. `cd` to `/backend`, run ```node index.js``` to start the backend server at http://localhost:8080
5. `cd` to `/frontend`, run `yarn` or `npm` to install the dependencies.
6. Link the dependencies.
    ```bash
    cd ios && pod install
    ```
7. `cd` to `/frontend/src/configs/AppConfig.ts` and add your `GOOGLE_API_KEY`
8. Now `cd` to the root directory and start the application.
    ```bash
    yarn react-native start
    ```
9. Run simulator or devices using `Xcode`(iOS) or `Android Studio`(Android).

### Note:
- Test on actual devices: can access sensor data, but cannot connect backend localhost api.
- Test on simulator/ emulator: can connect backend localhost api, but cannot access sensor data
- If there is Network Error on Android emulator, try to change the API_URL to http://10.0.2.2:8080 instead of http://localhost:8080
