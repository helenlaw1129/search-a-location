import React from 'react';
import {Provider} from 'react-redux';
import SearchContainer from './src/features/Search/containers/SearchContainer';
import {store} from './src/store';

const App = (): JSX.Element => {
  return (
    <Provider store={store}>
      <SearchContainer />
    </Provider>
  );
};

export default App;
