import {scale} from 'react-native-size-matters';

const Color = {
  main: '#485bae',
  white: '#ffffff',
  textMain: '#44D7B6',
  darkGray: '#595365',
  mediumGray: '#949494',
  lightGray: '#BDBDBD',
};

const FontSize = {
  bigger: scale(24),
  big: scale(20),
  normal: scale(16),
  small: scale(14),
  smaller: scale(12),
  smallest: scale(10),
};
export default {Color, FontSize};
