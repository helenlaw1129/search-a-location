const AppConfig = {
  API_URL: 'http://localhost:8080',
  API_VERSION: '/api/v1',
  GOOGLE_API_KEY: '',
  IS_DEBUG: true,
};

export default AppConfig;
