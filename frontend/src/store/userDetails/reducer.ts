import {GooglePlaceData} from 'react-native-google-places-autocomplete';
import {SensorData} from 'react-native-sensors';
import {AnyAction} from 'redux';
import {
  UPDATE_USER_ID,
  UPDATE_SEARCH_TERM,
  UPDATE_SELECTED_LOCATION,
  UPDATE_SENSOR_TYPE,
  UPDATE_SENSOR_DATA,
  UPDATE_SESSION,
  UPDATE_APP_STATE,
  CLEAR_USER_DETAILS,
} from './actionTypes';

export type UserDetailsState = {
  userId: number | null;
  searchTerm: string;
  selectedLocation: GooglePlaceData | null;
  sensorType: string;
  sensorData: SensorData | null;
  sessionStart: string | null;
  curAppState: any | null;
};

const initialState: UserDetailsState = {
  userId: null,
  searchTerm: '',
  selectedLocation: null,
  sensorType: '',
  sensorData: null,
  sessionStart: null,
  curAppState: null,
};

const reducer = (
  state: UserDetailsState = initialState,
  action: AnyAction,
): UserDetailsState => {
  switch (action.type) {
    case UPDATE_USER_ID:
    case UPDATE_SEARCH_TERM:
    case UPDATE_SELECTED_LOCATION:
    case UPDATE_SENSOR_TYPE:
    case UPDATE_SENSOR_DATA:
    case UPDATE_SESSION:
    case UPDATE_APP_STATE:
      return {...state, ...action.payload};
    case CLEAR_USER_DETAILS:
      return {...initialState};
    default:
      return state;
  }
};

export default reducer;
