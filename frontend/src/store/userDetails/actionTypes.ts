const NAMESPACE = '@appState';

export const UPDATE_USER_ID = `${NAMESPACE}/updateUserId`;
export const UPDATE_SEARCH_TERM = `${NAMESPACE}/updateSearchTerm`;
export const UPDATE_SELECTED_LOCATION = `${NAMESPACE}/updateSelectedLocation`;
export const UPDATE_SENSOR_TYPE = `${NAMESPACE}/updateSensorType`;
export const UPDATE_SENSOR_DATA = `${NAMESPACE}/updateSensorData`;

export const UPDATE_SESSION = `${NAMESPACE}/updateSession`;
export const UPDATE_APP_STATE = `${NAMESPACE}/updateAppState`;

export const CLEAR_USER_DETAILS = `${NAMESPACE}/clearUserDetails`;
