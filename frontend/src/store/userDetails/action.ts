import {Dispatch} from 'redux';
import {
  UPDATE_USER_ID,
  CLEAR_USER_DETAILS,
  UPDATE_SEARCH_TERM,
  UPDATE_SELECTED_LOCATION,
  UPDATE_SENSOR_TYPE,
  UPDATE_SENSOR_DATA,
  UPDATE_SESSION,
  UPDATE_APP_STATE,
} from './actionTypes';
import {UserDetailsState} from './reducer';
import {DispatchReturnType} from '../../models/dispatchReturnType';
import {GooglePlaceData} from 'react-native-google-places-autocomplete';
import {SensorData} from 'react-native-sensors';

const getActionType = (key: keyof UserDetailsState): string => {
  switch (key) {
    case 'userId':
      return UPDATE_USER_ID;
    case 'searchTerm':
      return UPDATE_SEARCH_TERM;
    case 'curAppState':
      return UPDATE_APP_STATE;
    case 'selectedLocation':
      return UPDATE_SELECTED_LOCATION;
    case 'sensorType':
      return UPDATE_SENSOR_TYPE;
    case 'sensorData':
      return UPDATE_SENSOR_DATA;
    case 'sessionStart':
      return UPDATE_SESSION;
    default:
      return '';
  }
};

export type UpdateUserDetailsDispatchType = Dispatch<{
  type: string;
  payload: UserDetailsState;
}>;

export const updateUserDetails =
  (
    key: keyof UserDetailsState,
    value: string | number | GooglePlaceData | SensorData,
  ) =>
  (
    dispatch: Dispatch,
  ): ReturnType<UpdateUserDetailsDispatchType> | DispatchReturnType =>
    dispatch({
      type: getActionType(key),
      payload: {[key]: value},
    });

export const clearUserDetails =
  () =>
  (dispatch: Dispatch): Omit<UpdateUserDetailsDispatchType, 'payload'> =>
    dispatch({
      type: CLEAR_USER_DETAILS,
    });
