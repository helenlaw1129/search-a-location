import {
  applyMiddleware,
  combineReducers,
  createStore,
  Middleware,
  StoreEnhancer,
} from 'redux';
import thunkMiddleware from 'redux-thunk';
import userDetails, {UserDetailsState} from './userDetails/reducer';

export const rootReducer = combineReducers({
  userDetails,
});

export interface RootState {
  userDetails: UserDetailsState;
}

const bindMiddleware = (middleware: Middleware[]): StoreEnhancer => {
  return applyMiddleware(...middleware);
};

export const store = createStore(
  rootReducer,
  bindMiddleware([thunkMiddleware]),
);

export default rootReducer;
