import {GooglePlaceData} from 'react-native-google-places-autocomplete';
import {SensorData} from 'react-native-sensors';

export type DispatchReturnType = {
  type: string;
  payload: {[x: string]: string | number | GooglePlaceData | SensorData};
};
