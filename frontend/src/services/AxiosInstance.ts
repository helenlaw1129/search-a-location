import axios from 'axios';
import {Alert} from 'react-native';
import AppConfig from '../configs/AppConfig';

const instance = axios.create({
  baseURL: AppConfig.API_URL + AppConfig.API_VERSION,
  timeout: 5000,
  headers: {'Content-Type': 'application/json'},
});

export const setToken = (token: string) => {
  instance.defaults.headers.common.Authorization = `${token}`;
};

instance.interceptors.request.use(
  config => {
    console.log('[AXIOS-REQUEST]: ', config);
    return config;
  },
  error => {
    console.log('[AXIOS-REQUEST-ERROR]: ', error, error.response.data);

    return Promise.reject(error);
  },
);

// Overrided in RootNavigator effect
instance.interceptors.response.use(
  function (response) {
    console.log(
      '[AXIOS-RESPONSE]: ',
      `$$$method$$$ = ${response.config.method}, $$$url$$$ = ${
        (response.config.baseURL, response.config.url)
      }, $$$header$$$ = ${JSON.stringify(
        response.headers,
      )}, $$$data$$$ = ${JSON.stringify(response.data.data)}`,
    );

    return response;
  },
  function (error) {
    console.log(
      '[AXIOS-RESPONSE-ERROR]: ',
      error,
      '$$$url$$$ = ',
      (error.baseURL, error.url),
    );
    Alert.alert(error.name, error.message);
    return Promise.reject(error);
  },
);

export const Axios = instance;
