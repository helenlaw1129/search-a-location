import {Axios} from './AxiosInstance';

const createUser = () => Axios.post('/user', {});
const createSearchTermLog = (userId: number, searchTerm: string) =>
  Axios.post('/search', {userId, searchTerm});
const createSelectedLocationLog = (userId: number, locationSelected: string) =>
  Axios.post('/location', {userId, locationSelected});
const createSensorInfo = (
  userId: number,
  sensorType: string,
  sensorData: string,
) => Axios.post('/session', {userId, sensorType, sensorData});
const createSession = (userId: number, sessionTime: number) =>
  Axios.post('/session', {userId, sessionTime});

export default {
  createUser,
  createSearchTermLog,
  createSelectedLocationLog,
  createSensorInfo,
  createSession,
};
