import React, {memo} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import {scale} from 'react-native-size-matters';
import AppDesign from '../../../constants/AppDesign';

interface Props {
  location: string;
}

const LocationDescription = memo(({location}: Props) => (
  <View>
    <Text style={styles.selectedDescription}>You have selected:</Text>
    <Text style={styles.selectedLocation}>{location}</Text>
  </View>
));

const styles = StyleSheet.create({
  selectedDescription: {
    fontSize: AppDesign.FontSize.normal,
    fontWeight: '700',
    color: AppDesign.Color.main,
    paddingVertical: scale(12),
    textAlign: 'center',
  },
  selectedLocation: {
    fontSize: AppDesign.FontSize.small,
    fontWeight: '700',
    color: AppDesign.Color.darkGray,
    paddingVertical: scale(8),
    textAlign: 'center',
  },
});

export default LocationDescription;
