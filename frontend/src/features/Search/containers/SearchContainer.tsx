import React, {useCallback, useEffect, useRef} from 'react';
import {StyleSheet, View, AppState, Text} from 'react-native';
import {scale} from 'react-native-size-matters';
import {
  GooglePlaceData,
  GooglePlacesAutocomplete,
} from 'react-native-google-places-autocomplete';
import moment from 'moment';
import DeviceInfo from 'react-native-device-info';
import {
  accelerometer,
  SensorData,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';
import {useDispatch, useSelector} from 'react-redux';
import AppDesign from '../../../constants/AppDesign';
import AppService from '../../../services/AppService';
import {RootState} from '../../../store';
import {updateUserDetails} from '../../../store/userDetails/action';
import Layout from '../../Layout/Layout';
import AppConfig from '../../../configs/AppConfig';
import LocationDescription from '../components/LocationDescription';
import {usePrevious} from '../../../helpers/HelperHook';
import {Subscription} from 'rxjs';

const SearchContainer = (): JSX.Element => {
  const searchRef = useRef(null);
  const {
    userId,
    searchTerm,
    selectedLocation,
    sensorData,
    curAppState,
    sessionStart,
  } = useSelector((state: RootState) => state.userDetails);
  const prevAppState = usePrevious(curAppState);
  const dispatch = useDispatch();
  useEffect(() => {
    AppService.createUser()
      .then(res => {
        const {id, created_at} = res.data[0];
        dispatch(updateUserDetails('userId', id));
        dispatch(updateUserDetails('sessionStart', created_at));
      })
      .catch(err => {
        console.log('UserInfo err', err);
      });
  }, [dispatch]);

  const handleSearchInput = useCallback(
    (text: string) => {
      dispatch(updateUserDetails('searchTerm', text));
    },
    [dispatch],
  );

  useEffect(() => {
    const subcription = AppState.addEventListener(
      'change',
      handleAppStateChange,
    );

    return () => {
      subcription.remove();
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  useEffect(() => {
    setUpdateIntervalForType(SensorTypes.accelerometer, 1000);

    let accelerometerSubscription: Subscription | null = null;

    DeviceInfo.isEmulator().then(isEmulator => {
      if (!isEmulator) {
        accelerometerSubscription = accelerometer.subscribe({
          next: (data: SensorData) => {
            dispatch(updateUserDetails('sensorData', data));
          },
          error: err => {
            console.log('accelerometer err', err);
          },
        });
      }
      return () => {
        accelerometerSubscription?.unsubscribe();
      };
    });
  }, [dispatch]);

  useEffect(() => {
    if (!userId) {
      return;
    }
    if (
      prevAppState === 'active' &&
      (curAppState === 'background' || curAppState === 'inactive')
    ) {
      const sessionEnd = moment().format();
      const sessionDuration =
        moment(sessionEnd).unix() - moment(sessionStart).unix();
      AppService.createSession(userId, sessionDuration).then(() =>
        dispatch(updateUserDetails('sessionStart', '')),
      );
    }
    if (
      (prevAppState === 'inactive' || prevAppState === 'background') &&
      curAppState === 'active'
    ) {
      const newSession = moment().format();
      dispatch(updateUserDetails('sessionStart', newSession));
    }
  }, [curAppState, dispatch, prevAppState, sessionStart, userId]);

  const handleAppStateChange = useCallback(
    nextAppState => {
      dispatch(updateUserDetails('curAppState', nextAppState));
    },
    [dispatch],
  );

  const handleLocationSelect = useCallback(
    (data: GooglePlaceData) => {
      dispatch(updateUserDetails('selectedLocation', data));
      if (!userId) {
        return;
      }
      AppService.createSearchTermLog(userId, searchTerm);
      AppService.createSelectedLocationLog(userId, data.description);
    },
    [dispatch, searchTerm, userId],
  );

  const submitSearch = useCallback(() => {
    if (!userId) {
      return;
    }
    AppService.createSearchTermLog(userId, searchTerm);
  }, [searchTerm, userId]);

  return (
    <>
      <Layout />
      <View style={styles.searchSection}>
        <GooglePlacesAutocomplete
          placeholder="Search"
          onPress={handleLocationSelect}
          textInputProps={{
            placeholderTextColor: AppDesign.Color.mediumGray,
            onChangeText: handleSearchInput,
            onSubmitEditing: submitSearch,
            returnKeyType: 'search',
          }}
          listViewDisplayed={false}
          fetchDetails
          ref={searchRef}
          listEmptyComponent={() => (
            <Text style={styles.emptyText}>Nothing here...</Text>
          )}
          query={{
            key: `${AppConfig.GOOGLE_API_KEY}`,
            language: 'en',
          }}
          styles={{
            textInput: {
              borderWidth: 1,
              borderColor: AppDesign.Color.lightGray,
            },
            listView: {
              height: '100%',
            },
          }}
          onFail={err => console.log('errr', err)}
        />
      </View>
      <View style={styles.container}>
        <View />
        {selectedLocation ? (
          <LocationDescription location={selectedLocation.description} />
        ) : (
          <>
            <Text style={styles.selectedDescription}>Search a Location!</Text>
          </>
        )}
        <View style={styles.accelerometerInfoContainer}>
          <Text style={styles.accelerometerInfoTitle}>
            Your Accelerometer Info
          </Text>
          {sensorData ? (
            <Text style={styles.accelerometerData}>
              x: {Math.round(sensorData.x)} y: {Math.round(sensorData.y)} z:{' '}
              {Math.round(sensorData.z)}
            </Text>
          ) : (
            <Text style={styles.accelerometerData}>
              Sorry, this device doesn't support accelerometer
            </Text>
          )}
        </View>
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: AppDesign.Color.white,
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'column',
    flex: 1,
  },
  searchSection: {
    backgroundColor: AppDesign.Color.white,
    paddingHorizontal: scale(12),
    paddingVertical: scale(8),
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  emptyText: {
    padding: scale(8),
  },
  searchIcon: {
    padding: 10,
  },
  textInput: {
    width: '100%',
    padding: scale(8),
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: scale(12),
  },
  accelerometerInfoContainer: {
    width: '100%',
    backgroundColor: AppDesign.Color.lightGray,
    justifyContent: 'center',
    alignItems: 'center',
    padding: scale(24),
  },
  accelerometerInfoTitle: {
    color: AppDesign.Color.main,
    fontSize: AppDesign.FontSize.normal,
    fontWeight: '500',
  },
  accelerometerData: {
    textAlign: 'center',
    color: AppDesign.Color.white,
    fontSize: AppDesign.FontSize.small,
    fontWeight: '500',
    paddingTop: scale(8),
  },
  selectedDescription: {
    fontSize: AppDesign.FontSize.normal,
    fontWeight: '700',
    color: AppDesign.Color.main,
    paddingVertical: scale(12),
  },
});

export default SearchContainer;
