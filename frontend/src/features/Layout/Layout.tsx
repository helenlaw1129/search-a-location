import React, {memo} from 'react';
import {SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {scale} from 'react-native-size-matters';
import AppDesign from '../../constants/AppDesign';

const Layout = memo(() => (
  <SafeAreaView style={{backgroundColor: AppDesign.Color.main}}>
    <View style={styles.headerContainer}>
      <Text style={styles.headerTitle}>Search A Place</Text>
    </View>
  </SafeAreaView>
));

const styles = StyleSheet.create({
  headerContainer: {
    backgroundColor: AppDesign.Color.main,
    // flex: 1,
  },
  headerTitle: {
    color: AppDesign.Color.white,
    textAlign: 'center',
    fontSize: AppDesign.FontSize.normal,
    fontWeight: '700',
    paddingVertical: scale(12),
  },
});

export default Layout;
