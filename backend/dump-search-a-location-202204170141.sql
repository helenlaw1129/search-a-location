--
-- PostgreSQL database dump
--

-- Dumped from database version 14.2
-- Dumped by pg_dump version 14.2

-- Started on 2022-04-17 01:41:07 EDT

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 3 (class 2615 OID 2200)
-- Name: public; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA public;


ALTER SCHEMA public OWNER TO postgres;

--
-- TOC entry 3646 (class 0 OID 0)
-- Dependencies: 3
-- Name: SCHEMA public; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON SCHEMA public IS 'standard public schema';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 209 (class 1259 OID 16501)
-- Name: location_selection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.location_selection (
    id integer NOT NULL,
    location_selected character varying NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.location_selection OWNER TO postgres;

--
-- TOC entry 210 (class 1259 OID 16508)
-- Name: place_selection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.place_selection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.place_selection_id_seq OWNER TO postgres;

--
-- TOC entry 3647 (class 0 OID 0)
-- Dependencies: 210
-- Name: place_selection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.place_selection_id_seq OWNED BY public.location_selection.id;


--
-- TOC entry 211 (class 1259 OID 16509)
-- Name: place_selection_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.place_selection_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.place_selection_user_id_seq OWNER TO postgres;

--
-- TOC entry 3648 (class 0 OID 0)
-- Dependencies: 211
-- Name: place_selection_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.place_selection_user_id_seq OWNED BY public.location_selection.user_id;


--
-- TOC entry 212 (class 1259 OID 16510)
-- Name: search_term; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.search_term (
    id integer NOT NULL,
    search_term character varying NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.search_term OWNER TO postgres;

--
-- TOC entry 213 (class 1259 OID 16517)
-- Name: search_term_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.search_term_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.search_term_id_seq OWNER TO postgres;

--
-- TOC entry 3649 (class 0 OID 0)
-- Dependencies: 213
-- Name: search_term_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.search_term_id_seq OWNED BY public.search_term.id;


--
-- TOC entry 214 (class 1259 OID 16518)
-- Name: search_term_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.search_term_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.search_term_user_id_seq OWNER TO postgres;

--
-- TOC entry 3650 (class 0 OID 0)
-- Dependencies: 214
-- Name: search_term_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.search_term_user_id_seq OWNED BY public.search_term.user_id;


--
-- TOC entry 215 (class 1259 OID 16519)
-- Name: sensor_info; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.sensor_info (
    id integer NOT NULL,
    sensor_type character varying NOT NULL,
    sensor_data character varying NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.sensor_info OWNER TO postgres;

--
-- TOC entry 216 (class 1259 OID 16526)
-- Name: sensor_info_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sensor_info_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sensor_info_id_seq OWNER TO postgres;

--
-- TOC entry 3651 (class 0 OID 0)
-- Dependencies: 216
-- Name: sensor_info_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sensor_info_id_seq OWNED BY public.sensor_info.id;


--
-- TOC entry 217 (class 1259 OID 16527)
-- Name: sensor_info_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.sensor_info_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sensor_info_user_id_seq OWNER TO postgres;

--
-- TOC entry 3652 (class 0 OID 0)
-- Dependencies: 217
-- Name: sensor_info_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.sensor_info_user_id_seq OWNED BY public.sensor_info.user_id;


--
-- TOC entry 218 (class 1259 OID 16528)
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    username character varying,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16535)
-- Name: user_engagement; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.user_engagement (
    engagement_id integer NOT NULL,
    session_time integer NOT NULL,
    user_id integer NOT NULL,
    created_at timestamp without time zone DEFAULT now() NOT NULL,
    updated_at timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.user_engagement OWNER TO postgres;

--
-- TOC entry 220 (class 1259 OID 16540)
-- Name: user_engagement_engagement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_engagement_engagement_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_engagement_engagement_id_seq OWNER TO postgres;

--
-- TOC entry 3653 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_engagement_engagement_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_engagement_engagement_id_seq OWNED BY public.user_engagement.engagement_id;


--
-- TOC entry 221 (class 1259 OID 16541)
-- Name: user_engagement_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_engagement_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_engagement_user_id_seq OWNER TO postgres;

--
-- TOC entry 3654 (class 0 OID 0)
-- Dependencies: 221
-- Name: user_engagement_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_engagement_user_id_seq OWNED BY public.user_engagement.user_id;


--
-- TOC entry 222 (class 1259 OID 16542)
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- TOC entry 3655 (class 0 OID 0)
-- Dependencies: 222
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- TOC entry 3457 (class 2604 OID 16543)
-- Name: location_selection id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_selection ALTER COLUMN id SET DEFAULT nextval('public.place_selection_id_seq'::regclass);


--
-- TOC entry 3458 (class 2604 OID 16544)
-- Name: location_selection user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_selection ALTER COLUMN user_id SET DEFAULT nextval('public.place_selection_user_id_seq'::regclass);


--
-- TOC entry 3461 (class 2604 OID 16545)
-- Name: search_term id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.search_term ALTER COLUMN id SET DEFAULT nextval('public.search_term_id_seq'::regclass);


--
-- TOC entry 3462 (class 2604 OID 16546)
-- Name: search_term user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.search_term ALTER COLUMN user_id SET DEFAULT nextval('public.search_term_user_id_seq'::regclass);


--
-- TOC entry 3465 (class 2604 OID 16547)
-- Name: sensor_info id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor_info ALTER COLUMN id SET DEFAULT nextval('public.sensor_info_id_seq'::regclass);


--
-- TOC entry 3466 (class 2604 OID 16548)
-- Name: sensor_info user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor_info ALTER COLUMN user_id SET DEFAULT nextval('public.sensor_info_user_id_seq'::regclass);


--
-- TOC entry 3469 (class 2604 OID 16549)
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- TOC entry 3472 (class 2604 OID 16550)
-- Name: user_engagement engagement_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_engagement ALTER COLUMN engagement_id SET DEFAULT nextval('public.user_engagement_engagement_id_seq'::regclass);


--
-- TOC entry 3473 (class 2604 OID 16551)
-- Name: user_engagement user_id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_engagement ALTER COLUMN user_id SET DEFAULT nextval('public.user_engagement_user_id_seq'::regclass);


--
-- TOC entry 3627 (class 0 OID 16501)
-- Dependencies: 209
-- Data for Name: location_selection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.location_selection (id, location_selected, user_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3630 (class 0 OID 16510)
-- Dependencies: 212
-- Data for Name: search_term; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.search_term (id, search_term, user_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3633 (class 0 OID 16519)
-- Dependencies: 215
-- Data for Name: sensor_info; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.sensor_info (id, sensor_type, sensor_data, user_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3636 (class 0 OID 16528)
-- Dependencies: 218
-- Data for Name: user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public."user" (id, username, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3637 (class 0 OID 16535)
-- Dependencies: 219
-- Data for Name: user_engagement; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY public.user_engagement (engagement_id, session_time, user_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 3656 (class 0 OID 0)
-- Dependencies: 210
-- Name: place_selection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.place_selection_id_seq', 23, true);


--
-- TOC entry 3657 (class 0 OID 0)
-- Dependencies: 211
-- Name: place_selection_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.place_selection_user_id_seq', 1, false);


--
-- TOC entry 3658 (class 0 OID 0)
-- Dependencies: 213
-- Name: search_term_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.search_term_id_seq', 20, true);


--
-- TOC entry 3659 (class 0 OID 0)
-- Dependencies: 214
-- Name: search_term_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.search_term_user_id_seq', 1, false);


--
-- TOC entry 3660 (class 0 OID 0)
-- Dependencies: 216
-- Name: sensor_info_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sensor_info_id_seq', 1, true);


--
-- TOC entry 3661 (class 0 OID 0)
-- Dependencies: 217
-- Name: sensor_info_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.sensor_info_user_id_seq', 1, false);


--
-- TOC entry 3662 (class 0 OID 0)
-- Dependencies: 220
-- Name: user_engagement_engagement_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_engagement_engagement_id_seq', 3, true);


--
-- TOC entry 3663 (class 0 OID 0)
-- Dependencies: 221
-- Name: user_engagement_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_engagement_user_id_seq', 1, false);


--
-- TOC entry 3664 (class 0 OID 0)
-- Dependencies: 222
-- Name: user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.user_id_seq', 240, true);


--
-- TOC entry 3475 (class 2606 OID 16553)
-- Name: location_selection place_selection_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_selection
    ADD CONSTRAINT place_selection_pk PRIMARY KEY (id);


--
-- TOC entry 3477 (class 2606 OID 16555)
-- Name: search_term search_term_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.search_term
    ADD CONSTRAINT search_term_pk PRIMARY KEY (id);


--
-- TOC entry 3479 (class 2606 OID 16557)
-- Name: sensor_info sensor_info_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor_info
    ADD CONSTRAINT sensor_info_pk PRIMARY KEY (id);


--
-- TOC entry 3483 (class 2606 OID 16559)
-- Name: user_engagement user_engagement_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_engagement
    ADD CONSTRAINT user_engagement_pk PRIMARY KEY (engagement_id);


--
-- TOC entry 3481 (class 2606 OID 16561)
-- Name: user user_pk; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT user_pk PRIMARY KEY (id);


--
-- TOC entry 3484 (class 2606 OID 16562)
-- Name: location_selection place_selection_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.location_selection
    ADD CONSTRAINT place_selection_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 3485 (class 2606 OID 16567)
-- Name: search_term search_term_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.search_term
    ADD CONSTRAINT search_term_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 3486 (class 2606 OID 16572)
-- Name: sensor_info sensor_info_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.sensor_info
    ADD CONSTRAINT sensor_info_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


--
-- TOC entry 3487 (class 2606 OID 16577)
-- Name: user_engagement user_engagement_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.user_engagement
    ADD CONSTRAINT user_engagement_fk FOREIGN KEY (user_id) REFERENCES public."user"(id);


-- Completed on 2022-04-17 01:41:07 EDT

--
-- PostgreSQL database dump complete
--

