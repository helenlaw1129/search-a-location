import express from 'express';
import bodyParser from 'body-parser';
import expressSession from 'express-session';
import dotenv from 'dotenv';

dotenv.config();

// Initialize Knex
import Knex from 'knex';
const knexConfig = require('./knexfile');
const knex = Knex(knexConfig[process.env.NODE_ENV || "development"]);


//Server side code
const app = express();

app.use(expressSession({
    secret: 'location_search_app',
    resave: true,
    saveUninitialized: true
}))

app.use(bodyParser.urlencoded({ extended: true })); 
app.use(bodyParser.json())


// Initialize the controller and service for MVC architecture

//user routes
import { UserService } from "./service/UserService";
import { UserController } from "./controller/UserController";

const userService = new UserService(knex);
export const userController = new UserController(userService);

// search term routes
import { SearchTermService } from "./service/SearchTermService";
import { SearchTermController } from "./controller/SearchTermController";

const searchTermService = new SearchTermService(knex);
export const searchTermController = new SearchTermController(searchTermService);

// sensor info routes
import { SensorInfoService } from "./service/SensorInfoService";
import { SensorInfoController } from "./controller/SensorInfoController";

const sensorInfoService = new SensorInfoService(knex);
export const sensorInfoController = new SensorInfoController(sensorInfoService);

// location selection routes
import { LocationService } from "./service/LocationService";
import { LocationController } from "./controller/LocationController";

const locationService = new LocationService(knex);
export const locationController = new LocationController(locationService);


// session routes
import { SessionService } from "./service/SessionService";
import { SessionController } from "./controller/SessionController";

const sessionService = new SessionService(knex);
export const sessionController = new SessionController(sessionService);



import { routes } from './routes';
const API_VERSION = process.env.API_VERSION || '/api/v1'; 
app.use(API_VERSION, routes);


// Server listen to requests and feedback to client whether it is listening
const PORT = 8080;
app.listen(PORT, () => {
    console.log(`Listening at http://localhost:${PORT}/`);
});
