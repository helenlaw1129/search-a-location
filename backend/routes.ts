import express from 'express';
import {locationController, searchTermController, sensorInfoController, sessionController, userController} from './main';

export const routes = express.Router();

routes.post('/user', userController.addUser);
routes.post('/search', searchTermController.addSearchTerm);
routes.post('/location', locationController.addLocation);
routes.post('/sensor', sensorInfoController.addSensorInfo);
routes.post('/session', sessionController.addSession);
