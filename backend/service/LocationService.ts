import {Knex} from "knex";


export class LocationService {
    constructor(private knex: Knex) {}

    async addLocation(user_id: number, location_selected: string){
        return (await this.knex.insert({user_id, location_selected}).into('location_selection'));
    }
}
