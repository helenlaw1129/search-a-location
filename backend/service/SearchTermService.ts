import {Knex} from "knex";


export class SearchTermService {
    constructor(private knex: Knex) {}

    async addSearchTerm(user_id: number, search_term: string){
        return (await this.knex.insert({user_id, search_term}).into('search_term'));
    }
}
