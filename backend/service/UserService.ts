import {Knex} from "knex";

export class UserService {
    constructor(private knex: Knex) {}

    async addUser(username: string | null){
        return (await this.knex.insert({username}).into('user').returning('*'));
    }
}
