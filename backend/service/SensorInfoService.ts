import {Knex} from "knex";


export class SensorInfoService {
    constructor(private knex: Knex) {}

    async addSensorInfo(user_id: number, sensor_type: string, sensor_data: string){
        return (await this.knex.insert({user_id, sensor_type, sensor_data}).into('sensor_info'));
    }
}
