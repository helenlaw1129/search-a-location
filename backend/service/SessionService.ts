import {Knex} from "knex";


export class SessionService {
    constructor(private knex: Knex) {}

    async addSession(user_id: number, session_time: number){
        return (await this.knex.insert({user_id, session_time}).into('user_engagement').returning('user_id'));
    }
}
