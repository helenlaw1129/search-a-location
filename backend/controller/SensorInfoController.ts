import { Request, Response } from "express";
import { SensorInfoService } from "../service/SensorInfoService";

export class SensorInfoController {
  constructor(private sensorInfoService: SensorInfoService) {}

  addSensorInfo = async (req: Request, res: Response) => {
    try {
      const { userId, sensorType, sensorData } = req.body;
      const user_id = parseInt(userId);
      const sensor_type = sensorType;
      const sensor_data = sensorData;

      if (isNaN(user_id) || !sensorType || !sensorData) {
        res.status(400).json({ message: "Cannot find userId / sensorType / sensorData" });
        return;
      }

      await this.sensorInfoService.addSensorInfo(user_id, sensor_type, sensor_data);
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
