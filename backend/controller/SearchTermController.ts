import { Request, Response } from "express";
import { SearchTermService } from "../service/SearchTermService";

export class SearchTermController {
  constructor(private searchTermService: SearchTermService) {}

  addSearchTerm = async (req: Request, res: Response) => {
    try {
      const { userId, searchTerm } = req.body;
      const user_id = parseInt(userId);
      const search_term = searchTerm;

      if (isNaN(user_id) || !search_term) {
        res.status(400).json({ message: "Cannot find userId / searchTerm" });
        return;
      }

      await this.searchTermService.addSearchTerm(user_id, search_term);
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
