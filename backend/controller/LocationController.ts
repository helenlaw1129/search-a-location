import { Request, Response } from "express";
import { LocationService } from "../service/LocationService";

export class LocationController {
  constructor(private locationService: LocationService) {}

  addLocation = async (req: Request, res: Response) => {
    try {
      const { userId, locationSelected } = req.body;
      const user_id = parseInt(userId);
      const location_selected = locationSelected;

      if (isNaN(user_id) || !locationSelected) {
        res.status(400).json({ message: "Cannot find userId / locationSelected" });
        return;
      }

      await this.locationService.addLocation(user_id, location_selected);
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
