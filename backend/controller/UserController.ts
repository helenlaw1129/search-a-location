import { Request, Response } from "express";
import { UserService } from "../service/UserService";

export class UserController {
  constructor(private userService: UserService) {}

  addUser = async (req: Request, res: Response) => {
    try {
      const { username } = req.body;
      const result = await this.userService.addUser(username);
      res.json(result);
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
