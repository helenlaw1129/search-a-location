import { Request, Response } from "express";
import { SessionService } from "../service/SessionService";

export class SessionController {
  constructor(private sessionService: SessionService) {}

  addSession = async (req: Request, res: Response) => {
    try {
      const { userId, sessionTime } = req.body;
      const user_id = parseInt(userId);
      const session_time = parseInt(sessionTime);

      if (isNaN(user_id) || isNaN(session_time)) {
        res.status(400).json({ message: "Cannot find userId / sessionTime" });
        return;
      }

      await this.sessionService.addSession(user_id, session_time);
      res.json({ success: true });
    } catch (err) {
      console.error(err.message);
      res.status(500).json({ message: "internal server error" });
    }
  };
}
